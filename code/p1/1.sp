* Ehsan Jahangirzadeh 810194554
* CA5 VLSI

**** Load libraries ****
.inc '45nm_HP.pm'

**** Parameters ****
.param			Lmin=45n
+beta=2
+Wp='2*Lmin*beta'
+Wn='2*Lmin'
+Vdd=1V

**** Source Voltage ****

VSupply		Vs		gnd		DC		  Vdd
*Vin1		  word	gnd		pulse	  0	    Vdd		200p		10p		10p		200p	400p
Vin2		  bit		gnd		DC		  Vdd
Vin3		  bitb	gnd		DC		  0

**** Subkits ****

**** Circuit ****

Mp1		n1		  n2		    Vs			Vs		  pmos	  l='Lmin'		w='Wp'	
Mp2		n2		  n1		    Vs			Vs		  pmos	  l='Lmin'		w='Wp'
Mn1		n1    	n2  		  gnd   	gnd   	nmos  	l='Lmin' 		w='Wn'
Mn2		n2    	n1  		  gnd   	gnd   	nmos  	l='Lmin' 		w='Wn'
Mn3		bit    	word  	  n1   		n1   	  nmos  	l='Lmin' 		w='Wn'
Mn4		bitb    word  	  n2   		n2   	  nmos  	l='Lmin' 		w='Wn'
C1		bit		  gnd		    10fF
C2		bitb	  gnd		    10fF

*READ
*.IC V(n1)=0 V(n2)=Vdd V(bit)=Vdd V(bitb)=Vdd

*WRITE
*.IC V(n1)=0 V(n2)=Vdd 

**** Analysis ****

.tran 	10p 500p 10p


**** Measurements ****

.meas		tran	AVGpower avg	Power	from=0		to=500p

.END